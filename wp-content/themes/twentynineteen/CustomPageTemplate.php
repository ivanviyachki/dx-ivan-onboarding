<?php /* Template Name: CustomPageTempalte */ ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

                get_template_part( 'template-parts/content/content', 'page' );

                the_time('m/j/y g:i A');


			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

