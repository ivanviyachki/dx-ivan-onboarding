<?php
/**
 * Template Name: Archives Student
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
get_header();
?>

    <section id="primary" class="content-area">
        <main id="main" class="site-main">
                                                
            <?php if ( have_posts() ) : ?>

            <header class="page-header">
                <?php
                    the_archive_title( '<h1 class="page-title">', '</h1>' );
                ?>
            </header>    

                <?php
                $args = array( 'post_type' => 'student',
                               'meta_key' => 'active-student-checkbox',
                               'meta_value' => '1' );
                $my_query = new WP_Query( $args );

                if( $my_query->have_posts() ) {     
                    $counter = 0;
                    $posts_count = 3;
                    
                    while( $my_query->have_posts() && $counter <= $posts_count ) {        
                        $my_query->the_post();
                        get_template_part( 'template-parts/content/content', 'excerpt' );
                        $counter++;
                    }
                }
                twentynineteen_the_posts_navigation();
                wp_reset_postdata();
                else :
                    get_template_part( 'template-parts/content/content', 'none' );
                endif;
            ?>

		</main>
	</section>
    
<?php
get_footer();