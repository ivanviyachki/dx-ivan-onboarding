<?php
   /*
   Plugin Name: Second My Onboarding Plugin
   Plugin URI: http://my-awesomeness-emporium.com
   description: Second  On Boarding Plugin 
  a plugin to create awesomeness and spread joy
   Version: 1.2
   Author: Ivan Viyachki
   Author URI: http://mrtotallyawesome.com
   License: GPL2
   */

   /**  
 * Adds a view to the post being viewed
 *
 * Finds the current views of a post and adds one to it by updating
 * the postmeta. The meta key used is "awepop_views".
 *
 * @global object $post The post object
 * @return integer $new_views The number of views the post has
 *
 */

require ( 'class-second-admin-menu.php' );
$second_admin_menu = new Second_admin_menu;

require ( 'class-second-ajax.php' );
$secon_ajax = new Second_ajax;

?> 