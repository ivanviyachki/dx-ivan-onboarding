jQuery(document).ready(function() {
    jQuery.ajax({
        url: ajaxurl,
        type : 'GET',
        data : {
            action : "display_link"
        },
        success: function(data) {
            jQuery('#your_site_frontend').html(data);
        }
    });
    jQuery('#submit_your_link').click(function(){
        var link = jQuery("#input_your_link").val();
        var transient_duration = jQuery("#transient_duration").val();
        jQuery.ajax({
            url: ajaxurl,
            type : 'POST',
            data : {
                action : "set_link",
                input : link,
                val : transient_duration
            },
            success: function(data) {
                jQuery('#your_site_frontend').html(data);
            }
        });
    });
});