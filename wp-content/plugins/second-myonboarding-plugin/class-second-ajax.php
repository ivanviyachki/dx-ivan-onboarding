<?php
if( ! class_exists( 'Second_ajax' ) ){

    class Second_ajax{
        public function __construct() {
            add_action( 'wp_ajax_set_link', array( $this,'set_link' ) );
            add_action( 'wp_ajax_nopriv_set_link', array( $this,'set_link' ) );
            add_action( 'wp_ajax_display_link', array( $this,'display_link' ) );
            add_action( 'wp_ajax_nopriv_display_link', array( $this,'display_link' ) );

        }

        function display_link(){
            if ( !current_user_can( 'manage_options' ) )  {
                wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
            }

            if ( false === ( $value = get_transient( 'link_result' ) ) ) {
                echo  "There is no page";
                
            } else {
                echo $value;
            }
        }

        function set_link() {
            if ( !current_user_can( 'manage_options' ) )  {
                wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
            }

            if ( isset( $_POST['input'] ) ) {
                $link = sanitize_text_field ( $_POST['input'] );
                $transient_duration = sanitize_text_field ( $_POST['val'] );

                if ( wp_http_validate_url( $link ) ) {
                    $homepage = file_get_contents( $link );
                    sanitize_text_field( $homepage );
                    set_transient( 'link_result', $homepage, $transient_duration );
                    echo $homepage;

                } else {
                    wp_die( __( 'Invalid URL' ) );
                } 
            }
        }
    }
}
?>