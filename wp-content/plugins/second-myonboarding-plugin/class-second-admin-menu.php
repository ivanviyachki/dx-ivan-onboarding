<?php
if( ! class_exists( 'Second_admin_menu' ) ) {

    class Second_admin_menu{
            
        public function __construct() {
            add_action( 'admin_menu', array( $this, 'display_link_menu' ) );
            add_action( 'admin_menu', array( $this, 'enqueue_script_second_myonboarding' ) );

        }

        function display_link_menu() {
            add_menu_page( 
            'Display Your Link Settings ',
            'Display Your Link', 
            'manage_options', 
            'mt-top-level-handle', 
            array( $this, 'input') );
        }

        function input() {
            if ( !current_user_can( 'manage_options' ) )  {
                wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
            }

            $view  = '<h1>Insert Your Link</h1>';
            $view .= '<input type="url" name="link" id="input_your_link"<br>';
            $view .= '<input type="submit" value="Submit" id="submit_your_link" >';
            $view .= '<select id="transient_duration">';
            $view .= '<option value="MINUTE_IN_SECONDS">1 minute</option>';
            $view .= '<option value="10 * MINUTE_IN_SECONDS">10 minutes</option>';
            $view .= '<option value="HOUR_IN_SECONDS">1 hour</option>';
            $view .= '<option value="DAY_IN_SECONDS">1 day</option>';
            $view .= '<option value="WEEK_IN_SECONDS">1 week</option>';
            $view .= '</select>';
            $view .= '<br>';
            $view .= '<div id = "your_site_frontend"></div>';

            echo $view;
        }

        function enqueue_script_second_myonboarding() {
            wp_enqueue_script( 'display-link', plugins_url( 'display-link.js', __FILE__ ), array( 'jquery' ) );
        }
    }
}
?>