<?php
if( ! class_exists( 'Student_Widget' ) ) {
    class Student_Widget extends WP_Widget {

        function __construct() {
            parent::__construct(
             
            // Base ID of your widget
            'studetn_widget', 
             
            // Widget name will appear in UI
            esc_html__('Student Widget', 'wpb_widget_domain'), 
             
            // Widget description
            array( 'description' => __( 'Sample widget based on WPBeginner Tutorial', 'wpb_widget_domain' ), ) 
            );

            add_action( 'widgets_init', array( $this, 'register' ) );
        }

        public function register() {
            register_widget( 'Student_Widget' );
        }

        public function widget( $args, $instance ) {
            $title = 'Student List';
     
            // before and after widget arguments are defined by themes
            echo $args['before_widget'];
            if ( ! empty( $title ) )
                echo $args['before_title'] . $title . $args['after_title'];
     
            // This is where you run the code and display the output
            // echo __( 'Hello, World!', 'student_list_widget_domain' );
            if( $instance['active-or-not'] == 'Active') {
                $query_args = array( 'post_type' => 'student',
                                     'meta_key' => 'active-student-checkbox',
                                     'meta_value' => '1' );
            }
            else if( $instance['active-or-not'] == 'Inactive' ) {
                $query_args = array( 'post_type' => 'student',
                                     'meta_key' => 'active-student-checkbox',
                                     'meta_value' => '0' );
            }
            $my_query = new WP_Query( $query_args );
            
            if( $my_query->have_posts() ) { 
                $counter = 1;
                $posts_count =  $instance[ 'posts-per-page' ];
                while($my_query->have_posts() && $counter <= $posts_count) {                 
                    $my_query->the_post();
                    // get_template_part( 'template-parts/content/content', 'excerpt' );
                    echo $my_query->post->post_title;
                    echo "<br>";
                    $counter++;
                }
            }
            
            wp_reset_postdata();
            echo $args['after_widget'];
        }
             
        // Widget Backend 
        public function form( $instance ) {
            if ( isset( $instance[ 'posts-per-page' ] ) ) {
                $posts_per_page = $instance[ 'posts-per-page' ];
            }
            else {
                $posts_per_page = 4;
            }
            if(  isset( $instance['active-or-not'] ) ) {
                $active_or_not =  $instance['active-or-not'];
            }
            else {
                $active_or_not = 'Active';
            }
            
            // Widget admin form
            ?>
    
            <select id="<?php echo $this->get_field_id('active-or-not'); ?>" name="<?php echo $this->get_field_name('active-or-not'); ?>" class="widefat">
            <option <?php selected( $instance['active-or-not'], 'Active'); ?> value="Active">Active</option>
            <option <?php selected( $instance['active-or-not'], 'Inactive'); ?> value="Inactive">Inactive</option> 
            </select>
    
            <p>
            <label for="<?php echo $this->get_field_id( 'posts-per-page' ); ?>"><?php _e( 'Posts per page:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'posts-per-page' ); ?>" name="<?php echo $this->get_field_name( 'posts-per-page' ); ?>" type="text" value="<?php echo esc_attr( $posts_per_page ); ?>" />
            </p>
    
    
    
            <?php
        }
         
        // Updating widget replacing old instances with new
        public function update( $new_instance, $old_instance ) {
            $instance = array();
            $instance['active-or-not'] = ( ! empty( $new_instance['active-or-not'] ) ) ?  strip_tags( $new_instance['active-or-not'] ) : 'Active';
            $instance['posts-per-page'] = ( ! empty( $new_instance['posts-per-page'] ) ) ? strip_tags( $new_instance['posts-per-page'] ) : '';
            return $instance;
        }

    }
}
?>