<?php
if( ! class_exists( 'Custom_Endpoints' ) ) {
    class Custom_Endpoints {

        public function __construct(){
            add_action( 'rest_api_init', array( $this, 'register_endpoint_student_all_data' ) );
            add_action( 'rest_api_init', array( $this, 'register_endpoint_get_student_by_id' ) );
            add_action( 'rest_api_init', array( $this, 'register_endpoint_create_student' ) );
            add_action( 'rest_api_init', array( $this, 'register_endpoint_update_student' ) );
            add_action( 'rest_api_init', array( $this, 'register_endpoint_delete_student' ) );
        }

        function authorization_status_code() {
            $status = 401;
    
            if( is_user_logged_in() ) {
                $status = 403;
            }
            return $status;
        }

        function edit_item_permissions_check( $request ) {

            if ( ! current_user_can( 'edit_others_posts' ) ) {
                return new WP_Error( 'rest_forbidden', esc_html__( 'You cannot  edit the post resource.' ), array( 'status' => authorization_status_code() ) );
            }
            return true;
        }

        function register_endpoint_student_all_data() {
            register_rest_route( 'wp/v1', '/all-students', array(
                'methods' => 'GET',
                'callback' => array( $this, 'student_all_data' ),
            ) );
        }

        function student_all_data( $data ) {
            $students = get_posts( array(
                'post_type' => 'student',
            ) );
 
            if ( empty( $students ) ) {
                return null;
            }
            return $students;
        }

        function register_endpoint_get_student_by_id() {
            register_rest_route( 'wp/v1', '/get-student/(?P<id>\d+)', array(
                'methods' => 'GET',
                'callback' => array( $this, 'get_student_by_id' ),
            ) );        
        }

        function get_student_by_id( $data ) {
            $return = $data->has_valid_params();

            if ( $return == true ) {
                $student = get_post( intval( $data['id'] ) );
    
                if ( empty( $student ) || $student->post_type != 'student' ) {
                    return null;
                }
                return $student;
            } else {
                return $return;
            }

        }

        function register_endpoint_create_student() {
            register_rest_route( 'wp/v1', '/create-student', array(
                'methods' => 'POST',
                'callback' => array( $this, 'create_student' ),
                'permission_callback' => array( $this, 'edit_item_permissions_check' ),
            ) );
        }

        function create_student( $data ) {   
            $return = $data->has_valid_params();

            if ( $return == true) {                          
                $my_post = array(
                    'post_title' => strval( $data->get_param( 'name' ) ),
                    'post_type' => 'student',
                    'post_excerpt' => strval( $data->get_param( 'excerpt' ) ),
                    'meta_input' => array(
                        'student-country-city' => strval( $data->get_param( 'country_city' ) ),
                        'student-address' => strval( $data->get_param( 'address' ) ),
                        'student-birthday' => strtotime( $data->get_param( 'birthday' ) ),
                        'student-class' => intval( $data->get_param( 'class' ) ),
                    ), 
                );
                return wp_insert_post( $my_post );
            } else {
                return $return;
            }

        }

        function register_endpoint_update_student() {
            register_rest_route( 'wp/v1', '/update-student', array(
                'methods' => 'POST',
                'callback' => array( $this, 'handle_call' ),
                'permission_callback' => array( $this, 'edit_item_permissions_check' ),
            ) );
        }

        function update_student( $data ) {
            $status = $data->has_valid_params();

            if( $status == true) {
                $my_post = array(
                    'ID' => intval( $data->get_param( 'ID' ) ),
                    'post_title' => strval( $data->get_param( 'name' ) ),
                    'post_type' => 'student',
                    'post_excerpt' => strval( $data->get_param( 'excerpt' ) ),
                    'meta_input' => array(
                        'student-country-city' => strval( $data->get_param( 'country_city' ) ),
                        'student-address' => strval( $data->get_param( 'address' ) ),
                        'student-birthday' => strtotime( $data->get_param( 'birthday' ) ),
                        'student-class' => intval( $data->get_param( 'class' ) ),
                    ), 
                );
                return wp_update_post( $my_post );
            } else {
                return $status;
            }
        }

        function register_endpoint_delete_student() {
            register_rest_route( 'wp/v1', '/delete-student', array(
                'methods' => 'POST',
                'callback' => array( $this, 'delete_student' ),
                'permission_callback' => array( $this, 'edit_item_permissions_check' ),
            ) );
        }

        function delete_student( $data ) {
            $status = $data->has_valid_params();

            if( $return == true ) {
                return wp_delete_post( intval( $data->get_param( 'ID' ) ) );
            } else {
                return $status;
            }
        }
    }
}
?>