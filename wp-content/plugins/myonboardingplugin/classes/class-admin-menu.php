<?php
if( ! class_exists( 'Admin_menu' ) ) {
    class Admin_menu{

        public function __construct() {
            add_action( 'admin_menu', array( $this,'onboarding_submenu' ) );
            add_action( 'admin_menu', array( $this,'enqueue_script_onboarding' ) );
        }

        function onboarding_submenu() {
            add_menu_page( 
                'My Onboarding Settings',
                'My Onboarding', 
                'manage_options', 
                'my-unique-identifier',
                array( $this, 'wpdocs_my_custom_submenu_page_callback')
            );
        }

        function wpdocs_my_custom_submenu_page_callback() {
            if ( !current_user_can( 'manage_options' ) )  {
                wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
            }

            echo '<h1>My Onboarding Custom Submenu Page</h1>';
            echo '<input type="checkbox" name="filter_status" id="checkbox">';
            echo '<label for="checkbox">Filters Enabled</label>';
        }

        function enqueue_script_onboarding() {
            wp_enqueue_script( 'onboarding', plugins_url( '../assets/js/onboarding.js', __FILE__ ), array( 'jquery' ) );
        }
    }
}
?>