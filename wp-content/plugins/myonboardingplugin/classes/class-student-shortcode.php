<?php
if( ! class_exists( 'Shortcode' ) ){
    class Student_Shortcode{
        
        public function __construct() {
            add_shortcode( 'student_shortcode', array( $this, 'student_shortcode' ) );
        }

        public function student_shortcode( $atts ) {
            $a = shortcode_atts( array( 'student_id' => '' ), $atts );
            $id = strval( $a['student_id'] );
            $args = array( 'post_type' => 'student',
                           'post__in' => array( $a['student_id'] )
            );
            $query = new WP_Query( $args );
            $student_name = '';
            $student_class = 0;
            $student_img_html = '';
            $return_html = '';
    
            if( $query->have_posts() ) {
                while( $query->have_posts() ){
                    $query->the_post();
            
                    $post_id =  $query->post->ID;
                    //Student thumbnail
                    $student_img_html = wp_get_attachment_image( get_post_thumbnail_id( $post_id ), array( '50', '50') );
                    // Student name
                    $student_name = $query->post->post_title;
                    // Student class
                    $student_class = get_post_meta( $post_id, 'student-class', true);         
            
                    if( $student_img_html == '' ) {
                        $return_html = '<p>No thumbnail set for this student</p>';
                    } else {
                        $return_html = $student_img_html;
                    }
                    if( $student_name == '' ) {
                        $return_html .= '<p>No name record for this student</p>';
                    } else {
                        $return_html .= '<p>Student Name: ' . $student_name . '</p>';
                    }
                    if( $student_class  == '' ) {
                        $return_html .= '<p>No class record for this student</p>';
                    } else {
                        $return_html .= '<p>In class: ' . $student_class . '</p>';
                    }
                    wp_reset_postdata();
                }
            } else {
                $return_html = 'No students found for the provided ID.';
            }
    
            return $return_html;
        }
    }
}
?>