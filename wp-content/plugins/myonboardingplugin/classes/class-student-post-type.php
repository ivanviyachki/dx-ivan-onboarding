<?php
if( ! class_exists( 'Student_post_type' ) ) {
    class Student_post_type{

        public function __construct() {
            add_action( 'init', array( $this, 'create_student_post_type' ) );
            add_action( 'init', array( $this, 'subject_taxonomies' ) );
            add_action( 'admin_enqueue_scripts', array( $this, 'active_student_checkbox_enqueue' ) );
            add_action( 'manage_student_posts_custom_column', array( $this, 'student_columns_content') , 10, 2 );

            add_filter( 'manage_edit-student_columns', array( $this, 'student_columns_head' ) );
        }

        public function subject_taxonomies() {
            $labels = array(
                'name'                       => esc_attr__( 'Subjects', 'my-onboarding-domain' ),
                'singular_name'              => esc_attr__( 'Subject', 'my-onboarding-domain' ),
                'menu_name'                  => esc_attr__( 'Subjects', 'my-onboarding-domain' ),
                'all_items'                  => esc_attr__( 'All Items', 'my-onboarding-domain' ),
                'parent_item'                => esc_attr__( 'Parent Item', 'my-onboarding-domain' ),
                'parent_item_colon'          => esc_attr__( 'Parent Item:', 'my-onboarding-domain' ),
                'new_item_name'              => esc_attr__( 'New Item Name', 'my-onboarding-domain' ),
                'add_new_item'               => esc_attr__( 'Add New Item', 'my-onboarding-domain' ),
                'edit_item'                  => esc_attr__( 'Edit Item', 'my-onboarding-domain' ),
                'update_item'                => esc_attr__( 'Update Item', 'my-onboarding-domain' ),
                'view_item'                  => esc_attr__( 'View Item', 'my-onboarding-domain' ),
                'separate_items_with_commas' => esc_attr__( 'Separate items with commas', 'my-onboarding-domain'  ),
                'add_or_remove_items'        => esc_attr__( 'Add or remove items','my-onboarding-domain'  ),
                'choose_from_most_used'      => esc_attr__( 'Choose from the most used', 'my-onboarding-domain'  ),
                'popular_items'              => esc_attr__( 'Popular Items', 'my-onboarding-domain'  ),
                'search_items'               => esc_attr__( 'Search Items', 'my-onboarding-domain'  ),
                'not_found'                  => esc_attr__( 'Not Found', 'my-onboarding-domain'  ),
                'no_terms'                   => esc_attr__( 'No items', 'my-onboarding-domain'  ),
                'items_list'                 => esc_attr__( 'Items list', 'my-onboarding-domain'  ),
                'items_list_navigation'      => esc_attr__( 'Items list navigation', 'my-onboarding-domain'  ),
            );
            $args = array(
                'labels'                     => $labels,
                'hierarchical'               => true,
                'public'                     => true,
                'show_ui'                    => true,
                'show_admin_column'          => true,
                'show_in_nav_menus'          => true,
                'show_tagcloud'              => true,
            );
            register_taxonomy( 'subjectcategories', array( 'student' ), $args );
        }
    

        function create_student_post_type() {
            $labels = array(
                'name'                => esc_attr__( 'Students', 'my-onboarding-domain' ),
                'singular_name'       => esc_attr__( 'Student', 'my-onboarding-domain' ),
                'menu_name'           => esc_attr__( 'Students', 'my-onboarding-domain' ),
                'parent_item_colon'   => esc_attr__( 'Parent Student', 'my-onboarding-domain' ),
                'all_items'           => esc_attr__( 'All Students', 'my-onboarding-domain' ),
                'view_item'           => esc_attr__( 'View Student', 'my-onboarding-domain' ),
                'add_new_item'        => esc_attr__( 'Add New Student', 'my-onboarding-domain' ),
                'add_new'             => esc_attr__( 'Add New', 'my-onboarding-domain' ),
                'edit_item'           => esc_attr__( 'Edit Student', 'my-onboarding-domain' ),
                'update_item'         => esc_attr__( 'Update Student', 'my-onboarding-domain' ),
                'search_items'        => esc_attr__( 'Search Student', 'my-onboarding-domain' ),
                'not_found'           => esc_attr__( 'Not studens found', 'my-onboarding-domain' ),
                'not_found_in_trash'  => esc_attr__( 'Not studens found in Trash', 'my-onboarding-domain' ),
            );
                
                
            $args = array(
                'label'               => esc_attr__( 'students', 'my-onboarding-domain' ),
                'description'         => esc_attr__( 'Students view', 'my-onboarding-domain' ),
                'labels'              => $labels,   
                'supports'            => array( 'title', 'excerpt', 'thumbnail', 'editor' ),
                'taxonomies'          => array( 'subjectcategories' ),
                'hierarchical'        => false,
                'public'              => true,
                'show_ui'             => true,
                'show_in_menu'        => true,
                'show_in_nav_menus'   => true,
                'show_in_admin_bar'   => true,
                'menu_position'       => 5,
                'show_in_rest'        => true,
                'can_export'          => true,
                'has_archive'         => true,
                'exclude_from_search' => false,
                'publicly_queryable'  => true,
                'capability_type'     => 'page',
            );
                
            register_post_type( 'student', $args );
        }


        function student_columns_head( $defaults ) {
            $defaults['active_student'] = 'Active Student';
            return $defaults;
        }

        function active_student_checkbox_enqueue( $hook ) {

            if( $hook == 'edit.php' ) {
                global $post_type;

                if( $post_type == 'student' ) {
                    wp_enqueue_script( 'active_student', plugins_url('../assets/js/active_student.js', __FILE__ ), array( 'jquery' ) );
                }
            }
        }

        function student_columns_content( $column_name, $post_id ) {

            if( 'active_student' === $column_name ) {
                $post_meta = boolval( get_post_meta( $post_id, 'active-student-checkbox', true ) );
                ?>
                <input type="checkbox" class="active-student-checkbox" name="student-checkbox" data-post-id="<?php echo $post_id ?>" <?php checked( $post_meta ); ?> >
                <label for="active-student-checkbox  <?php echo $post_id ?>">Active Student</label>  
                <?php
            }
        }
    }
}
?>