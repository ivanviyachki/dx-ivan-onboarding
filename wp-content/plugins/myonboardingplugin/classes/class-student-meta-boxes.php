<?php
if ( ! class_exists( 'Student_meta_box' ) ) {
    class Student_meta_box {
        
        public function __construct() {
            add_action( 'add_meta_boxes', array( $this, 'add_student_custom_meta_boxes' ) );
            add_action( 'save_post', array( $this, 'student_save_metaboxes' ) );
            add_filter( 'the_content', array( $this, 'student_metaboxes_content' ) );
        }
        
        function add_student_custom_meta_boxes() {
                    
            add_meta_box(
                'student-info-metabox',
                'Student Info',
                array( $this, 'render_student_info_metabox' ),
                'student',
                'side',
                'default'
            );
        }

        function render_student_info_metabox( $post ) {
            // generate a nonce field
            wp_nonce_field( 'student-posttype-nonce', '_student_nonce' );
            //get previously saved meta values (if any)
            $country_city = get_post_meta( $post->ID, 'student-country-city', true);
            $address = get_post_meta( $post->ID, 'student-address', true);
            $birthday = get_post_meta( $post->ID, 'student-birthday', true);
            $class = get_post_meta( $post->ID, 'student-class', true);
            

            ?>
        
            <p>
            <label for="student-country-city">Student country and city of birth:</label>
            <input type="text" id="student-country-city" name="student-country-city" class="widefat"
            value="<?php echo $country_city; ?>" placeholder="eg. Sofia, Bulgaria">
            </p>
        
            <p>
            <label for="student-address"> Student address: </label>
            <input type="text" id="student-address" name="student-address" class="widefat"
            value="<?php echo $address; ?>">
            </p>
        
            <p>
            <label for="student-birthday">Student birthday:</label>
            <input type="text" id="student-birthday" name="student-birthday" class="widefat"
            value="<?php echo $birthday; ?>"placeholder="eg. 2018/12/1">
            </p>
        
        
            <p>
            <label for="student-class">Student class:</label>
            <input type="text" id="student-class" name="student-class" class="widefat"
            value="<?php echo $class; ?>" placeholder="eg. 12">
            </p>

            <?php
        }

        function student_save_metaboxes( $post_id ) {    

            $is_valid_nonce = isset( $_POST['_student_nonce'] ) && wp_verify_nonce( $_POST['_student_nonce'], 'student-posttype-nonce' );
            //exit depending on the save status or if the nonce is not valid
           
            
            //checking for the values and performing necessary actions
            if ( isset( $_POST['student-country-city'] ) ) {
                update_post_meta( $post_id, 'student-country-city', sanitize_text_field( $_POST['student-country-city'] ) );
            }
            if ( isset( $_POST['student-address'] ) ) {
                update_post_meta( $post_id, 'student-address', sanitize_text_field( $_POST['student-address'] ) ) ;
            }
            if ( isset( $_POST['student-birthday'] ) ) {
                update_post_meta( $post_id, 'student-birthday', sanitize_text_field( $_POST['student-birthday'] ) ) ;
            }	
            if ( isset( $_POST['student-class'] ) ) {
                update_post_meta( $post_id, 'student-class', sanitize_text_field( $_POST['student-class'] ) );
            }
        }

        function student_metaboxes_content( $content ) {
            if ( is_singular( 'student' ) || is_post_type_archive( 'student' ) ) {
                $student_country_city 		= get_post_meta( get_the_ID(), 'student-country-city', true );
                $student_address   			= get_post_meta( get_the_ID(), 'student-address', true );
                $student_birthday      		= get_post_meta( get_the_ID(), 'student-birthday', true );
                $student_class        		= get_post_meta( get_the_ID(), 'student-class', true);
                // Check if there is a  date of birth set
                    
                $student_info = '<p><strong>' . esc_attr__( 'Student birthplace:' ) . '</strong><br>'. sanitize_text_field( $student_country_city ) . '</p>';
                $student_info .= '<p><strong>' . esc_attr__( 'Student address:' ) . '</strong><br>'. sanitize_text_field( $student_address ) . '</p>';
                $student_info .= '<p><strong>' . esc_attr__( 'Student date of birth:' ) . '</strong><br>'. sanitize_text_field($student_birthday) . '</p>';
                $student_info .= '<p><strong>' . esc_attr__( 'Student class:' ) . '</strong><br>'. sanitize_text_field( $student_class ) . '</p>';
                
                $content = $student_info . $content;
            }
            
            return $content;
        }
    }  
}