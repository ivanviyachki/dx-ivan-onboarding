<?php
if( ! class_exists( 'Ajax' ) ) {
   class Ajax{
   
      public function __construct() {
         add_action( 'wp_ajax_get_checkbox_status', array( $this, 'get_checkbox_status' ) );
         add_action( 'wp_ajax_nopriv_get_checkbox_status', array( $this, 'get_checkbox_status' ) );
         add_action( 'wp_ajax_update_checkbox_status', array( $this, 'update_checkbox_status' ) );
         add_action( 'wp_ajax_nopriv_update_checkbox_status', array( $this, 'update_checkbox_status' ) );
         add_action( 'wp_ajax_write_active_student_checkbox_to_db', array( $this, 'write_active_student_checkbox_to_db' ) );
         add_action( 'wp_ajax_nopriv_write_active_student_checkbox_to_db', array( $this, 'write_active_student_checkbox_to_db' ) );
      }

      function get_checkbox_status() {
         $user_id = get_current_user_id();
         $checkbox_value = get_user_meta( $user_id, 'checkbox_status', 'true' );

         if ( $checkbox_value == 0 || $checkbox_value == 1 ) {
            echo $checkbox_value ;
         }
      }

      function update_checkbox_status() {
         $checkbox_value = sanitize_text_field ( $_POST['checkbox_value'] );

         if ( $checkbox_value == 0  || $checkbox_value == 1 ) {
            $user_id = get_current_user_id();

            if ( $checkbox_value == 0 ) {
               update_user_meta( $user_id, 'checkbox_status', $checkbox_value, '1' );
            } else {
               update_user_meta( $user_id, 'checkbox_status', $checkbox_value, '0' );
            }
         }
      }

      function write_active_student_checkbox_to_db() {
         $input_string = strval( $_POST['student_checkbox_value'] );
         $post_id = intval( $_POST['post_id'] );

         if ( $input_string == "true" ) {
             update_post_meta( $post_id, 'active-student-checkbox', true );
         } else {
             delete_post_meta( $post_id, 'active-student-checkbox' );
         }
     }

   }
}
?>