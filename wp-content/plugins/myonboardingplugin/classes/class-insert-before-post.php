<?php
if( ! class_exists( 'Inster_after_post' ) ){
    class Inster_before_post{

        public function __construct() {
            add_filter( "the_content", array($this, "add_paragraph" ) );
            add_filter( "the_content", array($this, "auto_insert_after_post" ) );
            add_filter( "the_content", array($this, "student_sidebar" ) );

        }

        function get_checkbox_value() {
            $user_id = get_current_user_id();
            $checkbox_value = get_user_meta( $user_id, 'checkbox_status', 'true' );

            return  $checkbox_value;
        }

        function auto_insert_after_post( $content ){
            if ( is_singular( 'post' ) || is_post_type_archive( 'post' ) ) {

                if ( $this->get_checkbox_value() == 1 ) {
                    $content  = 'Onboarding Filter:' . $content;
                    $content .= 'by Ivan Viyachki';
                }
            }
            return $content;   
        }

        function add_paragraph( $content ) {  
            if ( is_singular( 'student' ) || is_post_type_archive( 'student' ) ){
                
                if ( $this->get_checkbox_value() == 1 ) {
                    $content = '<p></p>' . $content;
                }	
            }
            return $content;
        }
        
        function student_sidebar( $content ) { 

            if ( is_active_sidebar( 'student-sidebar' ) ) {
                dynamic_sidebar( 'student-sidebar' );
            }
            return $content;
        }
    }
}
?>