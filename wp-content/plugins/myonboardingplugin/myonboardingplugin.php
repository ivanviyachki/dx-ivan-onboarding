<?php
   /*
   Plugin Name: My Onboarding Plugin
   Plugin URI: http://my-awesomeness-emporium.com
   description: On Boarding Plugin 
  a plugin to create awesomeness and spread joy
   Version: 1.2
   Author: Ivan Viyachki
   Author URI: http://mrtotallyawesome.com
   License: GPL2
   */

   /**  
 * Adds a view to the post being viewed
 *
 * Finds the current views of a post and adds one to it by updating
 * the postmeta. The meta key used is "awepop_views".
 *
 * @global object $post The post object
 * @return integer $new_views The number of views the post has
 *
 */

require ( 'classes/class-insert-before-post.php' );
$insert_after_post = new Inster_before_post();

require ( 'classes/class-admin-menu.php' );
$admin_menu = new Admin_Menu();

require ( 'classes/class-ajax.php' );
$ajax = new Ajax();

require ( 'classes/class-student-post-type.php' );
$studen_post_type = new Student_post_type();

require ( 'classes/class-student-meta-boxes.php' );
$studen_meta_boxes = new Student_meta_box();

require ( 'classes/class-student-shortcode.php' );
$student_shortcode = new Student_Shortcode();

require ( 'classes/class-student-widget.php' );
$student_widget = new Student_Widget();

require ( 'classes/class-custom-endpoints.php' );
$custom_endpoints = new Custom_Endpoints();
?> 