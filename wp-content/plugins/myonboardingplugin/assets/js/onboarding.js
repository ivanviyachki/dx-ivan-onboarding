jQuery( document ).ready( function() {
    jQuery.ajax({
        url: ajaxurl,
        type : 'GET',
        data : {
            action : "get_checkbox_status"
        },
        success: function( data ) {
            if( data > 0 ) {
                jQuery( '#checkbox' ).prop( 'checked', true );
            }
            else {
                jQuery( '#checkbox' ).prop( 'checked', false );
            }
        }
    });
    jQuery('#checkbox').click(function() {
        var value = '0';
        if ( jQuery( '#checkbox').is( ":checked" ) ) {
            value = '1'
        }
        var data = {
            'action' : 'update_checkbox_status',
            'checkbox_value' : value
        };
        jQuery.post( ajaxurl, data );
    });
});