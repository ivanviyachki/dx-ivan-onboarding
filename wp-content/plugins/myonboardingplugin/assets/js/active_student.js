jQuery(document).ready(function($) {    
    jQuery(".active-student-checkbox").click(function(){
        $.ajax({ 
		 url: ajaxurl,
		 type : 'POST',
         data : { action : 'write_active_student_checkbox_to_db', 
         student_checkbox_value: jQuery(this).prop("checked"), 
         post_id: jQuery(this).data("post-id") },
		 success: function( response ) {
		 }
	       })
	    .fail(function(xhr, errorThrown) {
		console.log(errorThrown);
	    });
    });
});